@extends('layouts.app')

@section('content')
    <div class="flex-center position-ref full-height">
        <div class="content">
            <div id="twitch-events">
                <twitch-login
                        client-id="157r2wot34t7b041g7u1l2sxsbtcg3"
                        accept="application/vnd.twitchtv.v5+json"
                ></twitch-login>
            </div>
        </div>
    </div>
@endsection