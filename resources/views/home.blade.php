@extends('layouts.app')

@section('content')
<div class="container">
    <div class="justify-content-center">
            <div id="twitch-events">
                <twitch-dashboard
                        name="{{Auth::user()->name}}"
                        client-id="{!! env('TWITCH_CLIENT_ID') !!}"
                        accept="application/vnd.twitchtv.v5+json"
                ></twitch-dashboard>

                {{--authorization="OAuth rv29i3aph5b9res75g1boagj85qm81"--}}
            </div>
    </div>
</div>
@endsection
