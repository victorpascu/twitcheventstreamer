# TwitchEventStreamer

This is an exercise in linking Twitch authentication, embeds and events into a single small app using Laravel and Vue.js. 
Users can log in with Twitch, view their favorite channels and see channel events.

Working demo available at: https://unmorph.com/twitch-event-streamer (tested on desktop Chromium browsers )

## AWS deployment

Since the backend requirements are minimal, there are two simple approaches to deploy this application in the AWS cloud:
- A Laravel-compatible EC2 instance (managed by Elastic Beanstalk) combined with an RDS instance to provide database 
services, or
- A single Lightsail server with a LAMP stack handling everything in isolation. 
Looking at the primarily decoupled nature of cloud services, this becomes less viable as the app
scales, and the below section focuses on the first approach.

Down the line, one can consider an additional EB cluster to help with request rate limiting, a gateway server to handle
request routing to the original EB cluster, and moving the data to NoSQL clusters. 

## Scaling to millions
While it's hard to gauge this early on exactly where the bottlenecks would appear, there's three main areas to take
into account.

##1. Processing

### Stage 1 ( <1M users a day)
Due to the fact that the backend and SQL are running only lightweight authentication code,
the first cracks may start to appear when the sheer number of requests becomes overwhelming for even a trivial app,
likely when dealing with millions of users daily. The app would likely still be running, but very slowly.

### Stage 2 (1-10m users a day)
With numbers in the millions, it could make sense to add a login rate limiting system that limits requests to an amount 
that is deemed manageable for the available infrastructure. In this case, the app could ping a server (preferably running
on another EB cluster) that would signal back if the user can make a login request or not, stopping them from causing
the auth request and the callback handling. On the rate limiting server, successful requests would decrease the 
ticket count down to 0, but the count also replenishes automatically over time.

### Stage 3 (10m users a day and beyond)
The main EB cluster could also run via load balancing, with a gateway server redirecting requests through multiple,
automatically created EC2 instances based on CPU usage. This works until the gateway server becomes swarmed from
handling single requests, which is the point where one would likely invest into some of Amazon's more high-end routing
solutions.

##2. Data storage

On the data storage side, the code is even lighter, but problems can appear down the line.

### Stage 1 (<1M-10M users a day)
A regular MySQL instance running on an RDS cluster is well suited to handle this kind of volume. Given the simplicity
of the requests, it shouldn't be a problem.

### Stage 2 (10M users a day and beyond)
At this stage problems may start appearing, though RDS auto-scaling can likely handle them for a while. Beyond that,
it would be worth considering using something better suited for mass data storage - as the relational side of the data
is almost irrelevant in a single-table application, a NoSQL DB might fare better beyond this level.

### Guaranteed infinity!
If there were hypothetically no desire to use the data in any way, one could just stop saving users into the DB,
 authenticate them on the fly during the callback and let them be on their way, removing the data storage layer 
 entirely.
 
 ##3. Twitch
 If an app were to make the amount of requests being discussed, Twitch would rate limit it early on. Webhooks might
 help to a small degree (i.e. saving local data on events for the most popular channels and not polling Twitch at 
 all for them), but a business relying on this would likely need to make special arrangements for increased rates.
